// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NaziZombieGameMode.generated.h"

/**
 * 
 */
UCLASS()
class WORLDATWAR_API ANaziZombieGameMode : public AGameModeBase
{
	GENERATED_BODY()
public: 
	ANaziZombieGameMode();

protected:
	class ANaziZombieGameState* ZombieGameState;

	bool bHasLoadedSpawnPoints;
	TArray<class ANaziZombiePlayerSpawnPoint*> PlayerSpawnPoints;
	TArray<class ANaziZombieZombieSpawnPoint*> ZombieSpawnPoints;

	UPROPERTY(EditAnywhere, Category = "NaziZombieSettings")
		TSubclassOf<class ANaziZombieCharacter> PlayerClass;	
	UPROPERTY(EditAnywhere, Category = "NaziZombieSettings")
		TSubclassOf<class AZombieBase> ZombieClass;

	FTimerHandle TZombieSpawnHandle;

	uint16 ZombiesRemaining;

protected:
	void CalculateZombieCount();
	void SpawnZombie();

protected:
	virtual void BeginPlay() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;

	void SetSpawnPoints();
};
