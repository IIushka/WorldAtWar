// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "NaziZombieZombieSpawnPoint.generated.h"

/**
 * 
 */
UCLASS()
class WORLDATWAR_API ANaziZombieZombieSpawnPoint : public ATargetPoint
{
	GENERATED_BODY()
public:
	ANaziZombieZombieSpawnPoint();
};
