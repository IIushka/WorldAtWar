// Fill out your copyright notice in the Description page of Project Settings.


#include "NaziZombie/Useables/Barricade.h"

#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Net/UnrealNetwork.h"

ABarricade::ABarricade()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMeshComponent");
	RootComponent = MeshComp;

	CollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");

	RootComponent = MeshComp;
	Cost = 500;

	UIMessage += "Door [Cost: " + FString::FromInt(Cost) + "]";
	bIsUsed = false;
}

void ABarricade::BeginPlay()
{
	Super::BeginPlay();
	SetReplicates(true);
}

void ABarricade::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABarricade, bIsUsed);
}

void ABarricade::OnRep_BarricadeUsed()
{	
	CollisionMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if(OpenAnimation)
		MeshComp->PlayAnimation(OpenAnimation, false);
}

void ABarricade::Use(class ACharacterBase* Player)
{
	UE_LOG(LogTemp, Warning, TEXT("IN USE FUNCTION FOR %s"), *GetName());
	bIsUsed = true;
	OnRep_BarricadeUsed();
}
