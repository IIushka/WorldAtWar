// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NaziZombie/Useables/InteractableBase.h"
#include "Barricade.generated.h"

UCLASS()
class WORLDATWAR_API ABarricade : public AInteractableBase
{
	GENERATED_BODY()
	
public:
	ABarricade();

protected:
	UPROPERTY(Editanywhere, Category = "Nazi Zombie Settings")
		class USkeletalMeshComponent* MeshComp;

	UPROPERTY(Editanywhere, Category = "Nazi Zombie Settings")
		class UStaticMeshComponent* CollisionMesh;

	UPROPERTY(Editanywhere, Category = "Nazi Zombie Settings")
		class UAnimationAsset* OpenAnimation;

	UPROPERTY(Editanywhere, Category = "Nazi Zombie Settings")
		uint16 Cost;

	UPROPERTY(ReplicatedUsing = OnRep_BarricadeUsed)
		bool bIsUsed;

	UFUNCTION()
		void OnRep_BarricadeUsed();

protected:
	virtual void BeginPlay() override;
	virtual void Use(class ACharacterBase* Player) override;
};
