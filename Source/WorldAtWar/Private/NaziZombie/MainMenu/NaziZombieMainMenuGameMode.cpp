// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldAtWar/Public/NaziZombie/MainMenu/NaziZombieMainMenuGameMode.h"
#include "WorldAtWar/Public/NaziZombie/MainMenu/NaziZombieBeaconHostObject.h"

#include "OnlineBeaconHost.h"

ANaziZombieMainMenuGameMode::ANaziZombieMainMenuGameMode()
{
	HostObject = nullptr;
	Host = nullptr;
}

bool ANaziZombieMainMenuGameMode::CreateHostBeacon()
{
	Host = GetWorld()->SpawnActor<AOnlineBeaconHost>(AOnlineBeaconHost::StaticClass());
	if (Host)
	{
		UE_LOG(LogTemp, Warning, TEXT("SPAWNED AOnlineBeaconHost"));
		if (Host->InitHost())
		{
			Host->PauseBeaconRequests(false);
			UE_LOG(LogTemp, Warning, TEXT("INIT HOST"));
			HostObject = GetWorld()->SpawnActor<ANaziZombieBeaconHostObject>(ANaziZombieBeaconHostObject::StaticClass());
			if (HostObject)
			{
				Host->RegisterHost(HostObject);
				UE_LOG(LogTemp, Warning, TEXT("SPAWNED HOST OBJECT"));
				return true;
			}
		}
	}
	return false;
}

class ANaziZombieBeaconHostObject* ANaziZombieMainMenuGameMode::GetBeaconHost()
{
	return HostObject;
}